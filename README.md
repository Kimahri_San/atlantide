# Sistema Atlantide

Il Sistema Atlantide (SA) è un sistema estensibile per la gestione centralizzata della modulistica, con la possibilità di aggiungere funzioni, quali le comunicazioni interne.

## Tecnologie

Una lista non omnicomprensiva delle tecnologie utilizzate nella realizzazione:

* Stack LLMP
* CakePHP
* TeXLive

## Progetto

Il progetto è ambizioso, ma a volte la realizzazione pratica scarseggerà di conoscenze date dall'esperienza e aggirerà gli ostacoli con degli *hacks* di volta in volta più o meno adatti alla situazione.

## Informazioni

Il codice è rilasciato solo sotto forma valutativa e, per ora (nella fase di sviluppo), tutti i diritti rimangono riservati al proprietario **Riccardo Macoratti** contattabile all'indirizzo <kimahri_san@zoho.com>.


