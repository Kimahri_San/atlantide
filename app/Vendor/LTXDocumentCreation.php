<?php

# Copyright (c) 2014 Riccardo Macoratti <kimahri_san@zoho.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
# BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# LaTeX Document Creation Class
# Version: 0.1

class LTXDocumentCreation {

  private $begin;
  private $end;
  private $content;

  private $clean_ext = array(
    "aux",
    "toc",
    "log",
    "tex"
  );

  /**
   * Legge il file al percorso $path.
   * Ritorna il contenuto del file.
   * $path: il percorso del file.
   */
  private function read_file($path) {
    if (is_readable($path)) {
      $fid = fopen($path, "r");
      if ($fid) {
        $return = fread($fid, filesize($path));
        fclose($fid);
      }
    }
    return (isset($return) ? $return : null);
  }

  /**
   * $begin_path: path del file iniziale per comporre il documento.
   * $end_path: path del file finale per comporre il document.
   * Se $end_path non è specificato lo si considera nullo.
   */
  public function __construct($begin_path=null, $end_path=null) {
    $this->begin = $this->read_file($begin_path);
    $this->end = $this->read_file($end_path);
  }

  /**
   * Getters.
   */
  public function get_begin() {
    return $this->begin;
  }

  public function get_end() {
    return $this->end;
  }

  public function get_content() {
    return $this->content;
  }

  /**
   * Setters.
   */
  public function set_begin($path) {
    $this->begin = $this->read_file($path);
  }

  public function set_end($path) {
    $this->end = $this->read_file($path);
  }

  public function set_content($content) {
    $this->content = $content;
  }

  /**
   * Crea un file .tex per la compilazione.
   * $path: il percorso al quale crearlo.
   * $name: nome del file.
   */
  private function create_tex($path, $name) {
    if (!is_null($this->begin)) {
      $document = $this->begin;
      if (!is_null($this->content)) {
        $document .= $this->content;
      }
      if (!is_null($this->end)) {
        $document .= $this->end;
      }
      $fid = fopen($path."/".$name, "w");
      fwrite($fid, $document);
      fclose($fid);
      return true;
    }
    return false;
  }

  /**
   * Pulisce la directory di output dai file di complilazione indesiderati.
   * $path: percorso di output di compilazione.
   * $name: nome del file .tex da cui ricavare gli altri da eliminare.
   */
  private function clean($path, $name) {
    foreach ($this->clean_ext as $x) {
      unlink($path."/".$name.".".$x); 
    }
  }

  /**
   * Rudimentale controllo della riuscita della compilazione.
   * $log: log di pdlatex.
   */
  private function check($log) {
    return strpos($log, "!") === false;
  }

  /**
   * Compila il documento .tex che viene creato, controllando se la compilazione
   * riesce ed eliminando i file residui superflui.
   * $path: percorso di ouput dei file.
   * $name: nome del file .tex che verrà creato.
   */
  public function compile($path, $name) {
    if (file_exists($path)) {
      $name_noext = explode(".", $name, -1)[0];
      if ($this->create_tex($path, $name_noext.".tex")) {
        $log = shell_exec(
          "/usr/bin/pdflatex --output-directory=$path --interaction nonstopmode $path/$name_noext.tex"
        );
        if ($this->check($log)) {
          $this->clean($path, $name_noext);
          return $path."/".$name;
        }
      }
    } else {
      die("Error in compilation of LaTeX file. Retry.");
    }
  }

}

?>
